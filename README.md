# Rust to Go Hello World Comparison

![](rust-go.png)

![](basic-bm.png)

![](file-size.png)

No surprises here. Here are some initial conclusions when considering the two languages in the context of coding education:

* Rust requires `cargo new hello-rust` and specific workspace. Go doesn't
* Go is simpler to write and test in VSCode or from command line.
* Rust has a simpler raw syntax, initially, but gets more complicated.
* Go has a much simpler set of build artifacts (just the one executable).
* Rust buries the executable down in `target/debug`.
* Neither is anywhere small enough to be saved or run on Microchip PIC devices.
* Both would run cleanly on MIPS or ARM.

Other examinations about cross compiling, concurrency, memory management, testing and benchmarking will come later in other projects.
